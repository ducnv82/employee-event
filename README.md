# Employee & EventService uses Spring Boot
![Requirements](requirements.png)

# What has been done
All required parts, bonus parts: 

1.  Use Swagger (https://swagger.io) to expose employeeservice and eventservice endpoints.
2.  Add authentication to access create, update and delete employeeservice endpoints.


# Prerequisite

1. Installations: Install latest Oracle JDK 8, download at http://www.oracle.com/technetwork/java/javase/downloads/index.html
2. Install Apache Maven 3.6.1

# Run the application

1.  Use the command `mvn clean verify` in the project folder to build and run all unit tests (33) and integration tests (37)
2.  Use the command `mvn spring-boot:run` in the project folder to run the app
3.  We can use [Postman](https://www.getpostman.com/ "Postman") to test REST APIs, import the Postman collection [EmployeeEventService.postman_collection.json](EmployeeEventService.postman_collection.json).   
Steps:
    *   Run the API **Login** to obtain the JWT (Json Web Token) authentication token. username is **admin**, password is **admin**
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **createDepartment** and **Send**
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **createEmployee** and **Send**
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **updateEmployee** and **Send**
    *   Send a request to the API **findEmployeeById** 
    *   Copy the token to the request header **Authorization**, after **Bearer** of the API **deleteEmployeeById** and **Send**
    *   Send a request to the API **findAllEvents**
4.  Access the Swagger API documentation at http://localhost:8080/swagger-ui.html
5.  Access the embedded H2 database console at http://localhost:8080/h2-console     


# Notes about technologies
*   Spring Boot 2.1.4 for the app with Spring Rest, Spring Security, Json Web Token (jjwt), Spring Data JPA, Bean Validation 
*   Embedded database H2
*   Embedded message broker Apache ActiveMQ
*   Unit test with jmockit for mocking, integration test with REST Assured. Run unit tests in Eclipse: You must specify a JVM argument to jmockit.jar, my PC is: -javaagent:C:/Users/Duc/.m2/repository/org/jmockit/jmockit/1.46/jmockit-1.46.jar

# Notes about code architecture
*   All configuration classes are in the package com.mycompany.employeeevent.config. When the app is initialized, the class InitData will insert a user with username **admin** and password **admin** into the database, table **user**, to authenticate later.  
*   Event producer and consumer to and from JMS queue (Apache ActiveMQ) are implemented with Apache Camel, please look at the class **CamelRoutes**.  
Producer: `from("direct:employee-event").to("activemq:queue:EMPLOYEE.EVENT?transacted=true&lazyCreateTransactionManager=false")`,   
Consumer: `from("activemq:queue:EMPLOYEE.EVENT?transacted=true&lazyCreateTransactionManager=false").bean(eventService, "processEmployeeEvent")`    
*   Embedded message broker Apache ActiveMQ
*   Unit test with jmockit for mocking, integration test with REST Assured