package com.mycompany.employeeevent.model;

import java.time.LocalDate;
import java.util.UUID;

import com.mycompany.employeeevent.entity.Department;
import com.mycompany.employeeevent.entity.Employee;

import static java.util.UUID.randomUUID;

public abstract class BaseTest {

    protected UpdateEmployeeRequest createUpdateEmployeeRequest() {
        final UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest();
        updateEmployeeRequest.setId(randomUUID());
        updateEmployeeRequest.setBirthday(LocalDate.of(1900, 1, 1));
        updateEmployeeRequest.setDepartmentId(1l);
        updateEmployeeRequest.setEmail("robin.hood@aol.com");
        updateEmployeeRequest.setFullName("Robin Hood");
        return updateEmployeeRequest;
    }

    protected CreateEmployeeRequest createEmployeeRequest() {
        final CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest();
        createEmployeeRequest.setBirthday(LocalDate.of(1900, 1, 1));
        createEmployeeRequest.setDepartmentId(1l);
        createEmployeeRequest.setEmail("robin.hood@aol.com");
        createEmployeeRequest.setFullName("Robin Hood");
        return createEmployeeRequest;
    }

    protected Department createDepartment() {
        final Department department = new Department();
        department.setId(1l);
        department.setName("department 1");
        return department;
    }

    protected Employee createEmployee(final UUID id) {
        final Employee employee = new Employee();
        employee.setId(id);
        employee.setBirthday(LocalDate.of(1900, 1, 1));
        employee.setDepartmentId(1l);
        employee.setEmail("robin.hood@aol.com");
        employee.setFullName("Robin Hood");
        return employee;
    }
}
