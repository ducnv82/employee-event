package com.mycompany.employeeevent.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorFactory;
import javax.validation.ValidationException;

public abstract class BaseConstraintValidatorFactory implements ConstraintValidatorFactory {

    @Override
    public <T extends ConstraintValidator<?, ?>> T getInstance(final Class<T> key) {
        final T constraintValidator;

        try {
            constraintValidator = key.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ValidationException(e);
        }

        return constraintValidator;
    }

    @Override
    public void releaseInstance(final ConstraintValidator<?, ?> instance) {
    }

}
