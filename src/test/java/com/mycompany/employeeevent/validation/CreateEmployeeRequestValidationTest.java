package com.mycompany.employeeevent.validation;

import com.mycompany.employeeevent.model.CreateEmployeeRequest;
import org.junit.Test;

public class CreateEmployeeRequestValidationTest extends BaseValidatorTest {

    @Test
    public void testInvalidEmail() {
        final CreateEmployeeRequest createEmployeeRequest = createEmployeeRequest();
        createEmployeeRequest.setEmail("aaaa");
        assertViolationsForProperty(validator.<Object>validate(createEmployeeRequest), "email");
    }

    @Test
    public void testBlankEmail() {
        final CreateEmployeeRequest createEmployeeRequest = createEmployeeRequest();
        createEmployeeRequest.setEmail(" ");
        assertViolationsForProperty(validator.<Object>validate(createEmployeeRequest), "email");
    }

    @Test
    public void testNullDepartmentId() {
        final CreateEmployeeRequest createEmployeeRequest = createEmployeeRequest();
        createEmployeeRequest.setDepartmentId(null);
        assertViolationsForProperty(validator.<Object>validate(createEmployeeRequest), "departmentId");
    }

    @Test
    public void testValidCreateEmployeeRequest() {
        final CreateEmployeeRequest createEmployeeRequest = createEmployeeRequest();
        assertNoViolation(validator.<Object>validate(createEmployeeRequest));
    }
}
