package com.mycompany.employeeevent.service;

import com.mycompany.employeeevent.entity.Department;
import com.mycompany.employeeevent.model.CreateDepartmentRequest;
import com.mycompany.employeeevent.repository.DepartmentRepository;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DepartmentServiceImplTest {

    @Tested
    private DepartmentServiceImpl departmentService;

    @Injectable
    private DepartmentRepository departmentRepository;

    @Test
    public void testCreateDepartment() {
        final CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest("department1");

        departmentService.createDepartment(createDepartmentRequest);

        new Verifications() {{
            Department departmentToVerify;
            departmentRepository.save(departmentToVerify = withCapture());

            assertEquals("department1", departmentToVerify.getName());
        }};
    }
}
