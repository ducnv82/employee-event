package com.mycompany.employeeevent.controller;

import javax.validation.Valid;

import com.mycompany.employeeevent.model.LoginRequest;
import com.mycompany.employeeevent.model.LoginResponse;
import com.mycompany.employeeevent.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
@Api(description = "RESTful APIs for login's operation")
public class LoginController {

    @Autowired
    private UserService userService;

    @PostMapping
    @ApiOperation(value = "Login and obtain JSON Web Token", response = LoginResponse.class, produces = "application/json")
    public LoginResponse login(@ApiParam(value = "LoginRequest object", required = true)
                               @RequestBody @Valid final LoginRequest loginRequest) {

        return userService.login(loginRequest);
    }
}
