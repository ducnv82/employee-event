package com.mycompany.employeeevent.controller;

import javax.validation.Valid;

import com.mycompany.employeeevent.model.CreateDepartmentRequest;
import com.mycompany.employeeevent.model.Department;
import com.mycompany.employeeevent.service.DepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/api/departments")
@Api(description = "RESTful APIs for department's operations")
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @PostMapping
    @ResponseStatus(CREATED)
    @ApiOperation(value = "Create a department", response = Department.class, code = 201, produces = "application/json")
    public Department createDepartment(@ApiParam(value = "CreateDepartmentRequest object", required = true)
                                       @RequestBody @Valid final CreateDepartmentRequest request) {
        return departmentService.createDepartment(request);
    }
}
