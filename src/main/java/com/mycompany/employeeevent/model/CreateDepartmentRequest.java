package com.mycompany.employeeevent.model;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class CreateDepartmentRequest {

    @NotBlank
    @ApiModelProperty(name = "name", required = true, example = "department 1")
    private String name;

    public CreateDepartmentRequest() {}

    public CreateDepartmentRequest(@NotBlank final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
