package com.mycompany.employeeevent.model;

public class ErrorResponse {

    private final long timestamp;
    private final int status;
    private final String error;
    private final String message;
    private final String path;

    public ErrorResponse(final long timestamp, final int status, final String error, final String message,
                         final String path) {
        this.timestamp = timestamp;
        this.status = status;
        this.error = error;
        this.message = message;
        this.path = path;
    }

    public String getError() {
        return error;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }
}
