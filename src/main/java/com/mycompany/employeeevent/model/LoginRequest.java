package com.mycompany.employeeevent.model;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class LoginRequest {


    @NotBlank
    @ApiModelProperty(required = true, example = "admin", notes = "username to login. Please use admin")
    private String username;

    @NotBlank
    @ApiModelProperty(required = true, example = "admin", notes = "password to login. Please use admin")
    private String password;

    public LoginRequest() {}

    public LoginRequest(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }
}
