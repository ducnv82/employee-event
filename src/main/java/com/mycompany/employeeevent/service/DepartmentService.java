package com.mycompany.employeeevent.service;

import javax.validation.Valid;

import com.mycompany.employeeevent.model.CreateDepartmentRequest;
import com.mycompany.employeeevent.model.Department;

public interface DepartmentService {

    Department createDepartment(@Valid CreateDepartmentRequest request);
}
