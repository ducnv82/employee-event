package com.mycompany.employeeevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeEventServiceApplication {

    public static void main(final String[] args) {
        SpringApplication.run(EmployeeEventServiceApplication.class, args);
    }
}
