package com.mycompany.employeeevent.security.jwt;

import org.springframework.security.core.AuthenticationException;

public class InvalidJwtAuthenticationException extends AuthenticationException {

    public InvalidJwtAuthenticationException(final String e, final Throwable t) {
        super(e, t);
    }
}
