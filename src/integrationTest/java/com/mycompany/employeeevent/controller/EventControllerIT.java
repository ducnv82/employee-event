package com.mycompany.employeeevent.controller;


import java.time.LocalDateTime;

import com.google.common.collect.ImmutableMap;
import com.mycompany.employeeevent.EmployeeEventServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static java.time.LocalDateTime.parse;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmployeeEventServiceApplication.class, webEnvironment = DEFINED_PORT)
public class EventControllerIT extends BaseIT {

    @Test
    public void testFindAllEvents_empty() {
        given().contentType(JSON)
                .accept(JSON)
                .when().get("http://localhost:8080/api/events").then().statusCode(200)
                .body("size", equalTo(0));
    }

    @Test
    public void testFindAllEvents_orderByCreatedDate() {
        final String id = setupAndTestCreateEmployee();
        final ImmutableMap<String, Object>
                updateEmployeeRequest =
                ImmutableMap.of("birthday", "1905-05-05", "departmentId", 1, "email",
                                "robin.hood5@aol.com", "fullName", "Robin Hood 5", "id", id);

        given().body(updateEmployeeRequest).contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().put("http://localhost:8080/api/employees").then().statusCode(200)
                .body("id", equalTo(id))
                .body("email", equalTo(updateEmployeeRequest.get("email")))
                .body("fullName", equalTo(updateEmployeeRequest.get("fullName")))
                .body("departmentId", equalTo(updateEmployeeRequest.get("departmentId")))
                .body("birthday", equalTo(updateEmployeeRequest.get("birthday")));

        given().contentType(JSON)
                .accept(JSON).auth().oauth2(jwtToken)
                .when().delete("http://localhost:8080/api/employees/" + id).then().statusCode(200);

        final String event1CreatedDateString = given().contentType(JSON)
                .accept(JSON)
                .when().get("http://localhost:8080/api/events").then().statusCode(200)
                .body("size", equalTo(3))
                .body("[0]['eventType']", equalTo("CREATED"))
                .body("[0]['oldEmployee']", nullValue())
                .body("[0]['newEmployee'].id", notNullValue())
                .body("[0]['newEmployee'].email", equalTo(createEmployeeRequest.get("email")))
                .body("[0]['newEmployee'].fullName", equalTo(createEmployeeRequest.get("fullName")))
                .body("[0]['newEmployee'].departmentId", equalTo(createEmployeeRequest.get("departmentId")))
                .body("[0]['newEmployee'].birthday", equalTo(createEmployeeRequest.get("birthday")))
                .body("[0]['newEmployee'].department", nullValue())
                .body("[0]['createdDate']", notNullValue())
                .body("[1]['eventType']", equalTo("UPDATED"))
                .body("[1]['oldEmployee'].id", notNullValue())
                .body("[1]['oldEmployee'].email", equalTo(createEmployeeRequest.get("email")))
                .body("[1]['oldEmployee'].fullName", equalTo(createEmployeeRequest.get("fullName")))
                .body("[1]['oldEmployee'].departmentId", equalTo(createEmployeeRequest.get("departmentId")))
                .body("[1]['oldEmployee'].birthday", equalTo(createEmployeeRequest.get("birthday")))
                .body("[1]['oldEmployee'].department", nullValue())
                .body("[1]['createdDate']", notNullValue())
                .body("[1]['newEmployee'].id", notNullValue())
                .body("[1]['newEmployee'].email", equalTo(updateEmployeeRequest.get("email")))
                .body("[1]['newEmployee'].fullName", equalTo(updateEmployeeRequest.get("fullName")))
                .body("[1]['newEmployee'].departmentId", equalTo(updateEmployeeRequest.get("departmentId")))
                .body("[1]['newEmployee'].birthday", equalTo(updateEmployeeRequest.get("birthday")))
                .body("[1]['newEmployee'].department", nullValue())
                .body("[2]['eventType']", equalTo("DELETED"))
                .body("[2]['newEmployee']", nullValue())
                .body("[2]['oldEmployee'].id", notNullValue())
                .body("[2]['oldEmployee'].email", equalTo(updateEmployeeRequest.get("email")))
                .body("[2]['oldEmployee'].fullName", equalTo(updateEmployeeRequest.get("fullName")))
                .body("[2]['oldEmployee'].departmentId", equalTo(updateEmployeeRequest.get("departmentId")))
                .body("[2]['oldEmployee'].birthday", equalTo(updateEmployeeRequest.get("birthday")))
                .body("[2]['oldEmployee'].department", nullValue())
                .body("[2]['createdDate']", notNullValue())
                .extract().path("[0]['createdDate']");

        //assert order by createdDate.
        final LocalDateTime event1CreatedDate = parse(event1CreatedDateString);

        final String event2CreatedDateString = given().contentType(JSON).accept(JSON)
                .when().get("http://localhost:8080/api/events").then().extract().path("[1]['createdDate']");
        final LocalDateTime event2CreatedDate = parse(event2CreatedDateString);

        final String event3CreatedDateString = given().contentType(JSON).accept(JSON)
                .when().get("http://localhost:8080/api/events").then().extract().path("[2]['createdDate']");
        final LocalDateTime event3CreatedDate = parse(event3CreatedDateString);

        //Can use greaterThan because there are delays in create/update/delete processing and event insertion processing so event.createdDate are inserted into db with distinct values
        assertThat(event3CreatedDate, greaterThan(event2CreatedDate));
        assertThat(event2CreatedDate, greaterThan(event1CreatedDate));
    }
}
